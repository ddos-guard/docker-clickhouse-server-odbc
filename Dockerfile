FROM yandex/clickhouse-server:19.4.1.3

RUN set -x \
    && apt update -qq \
    && apt-get install -y unixodbc odbcinst odbc-postgresql \
    && apt-get autoclean -y \
    && apt-get autoremove -y \
    && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*